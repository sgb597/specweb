<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    $frame->assign('title', 'Browse Product Line');

    // Check for required information
    checkGet('s', 'p');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Populate the body template
    $products = backend_query(BACKEND_CMD_PRODUCT_MODELS . "&$_GET[s]&$_GET[p]");
    foreach ($products as $key => $value) {
       $products[$key] = explode('&', $value);
    }
    $body->assign('products', $products);

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.productline.htm'));
    if(is_file(PADDING_DIR . 'browse_productline'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'browse_productline'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
