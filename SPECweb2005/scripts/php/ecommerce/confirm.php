<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    // Check if session exists
    if (empty($_SESSION))
        error_page('Session does not exist');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Check for requirments
    if (!isset($_SESSION['email']) ||
        !isset($_SESSION['bill']) ||
        !isset($_SESSION['ship']) ||
        !isset($_SESSION['cart']))
        report("Missing session variables.", REPORT_ERROR);

    if(count($_SESSION['cart']) < 1)
        report("Cart is empty", REPORT_ERROR);

    checkGet('c', 's');

    $total = 0;
    foreach($_SESSION['cart'] as $key => $value)
        $total += $value * $_SESSION['price'][$key];

    $body->assign('total', number_format($total, 2));

    if (isset($_POST['confirm_submit'])) {
        $request= BACKEND_CMD_SUBMIT_ORDER . '&' . rawurlencode($_SESSION['email']);

        foreach ($_SESSION['ship'] as $ship_info)
            $request .= '&' . rawurlencode($ship_info);

        foreach ($_SESSION['bill'] as $bill_info)
            $request .= '&' . rawurlencode($bill_info);

        $request .= '&'. count($_SESSION['cart']);


        foreach ($_SESSION['cart'] as $item => $quantity){
            $request .= '&' . rawurlencode($item) . "&$quantity&" . count($_SESSION[$item]);
            foreach ($_SESSION[$item] as $selection)
                $request .= "&$selection";
        }

        $confirmation = backend_query($request);
        list($ship_date, $confirmation_num) = explode('&', $confirmation[0]);
        $body->assign('ship_date', $ship_date);
        $body->assign('conf_num', $confirmation_num);
        $frame->assign('title', 'Order Processed');
    } else
        $frame->assign('title', 'Verify order');

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.confirm.htm'));

    if(isset($_POST['confirm_submit'])){
        session_unset();
        session_destroy();
    }

    if(is_file(PADDING_DIR . 'confirm'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'confirm'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);
    renderPage();
?>
