<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    // Check if session exists
    if (empty($_SESSION))
        error_page('Session does not exist');

    $frame->assign('title', 'Your Cart');

    // Check for required information
    checkGet('c', 's');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Cart management
    if (isset($_GET['i'])){
        if (empty($_SESSION['cart'][$_GET['i']]))
            $_SESSION['cart'][$_GET['i']] = 1;
        else
            $_SESSION['cart'][$_GET['i']]++;
    } elseif (isset($_POST['cart_submit'])) {
        foreach ($_POST as $item => $quantity) {
            if ($item != 'cart_submit')
                $_SESSION['cart'][urldecode($item)] = $quantity;
            if ($quantity < 1)
                unset($_SESSION['cart'][urldecode($item)]);
        }
        if ($_POST['cart_submit'] == "checkout") {
            redirect("/login.php?{$_SERVER['QUERY_STRING']}&action=checkout", true);
        } elseif ($_POST['cart_submit'] == "SaveCart") {
            if (isset($_SESSION['email'])) {
                if (!isset($_GET['action']))
                    redirect("/cart.php?{$_SERVER['QUERY_STRING']}&action=SaveCart");
			}
            else
                redirect("/login.php?{$_SERVER['QUERY_STRING']}&action=SaveCart", true);
        }
    }

    // Save the cart to the backend
    if (isset($_GET['action']) && $_GET['action'] == "SaveCart") {
        $request=BACKEND_CMD_CARTSAVE .'&'. urlencode($_SESSION['email']) .'&'. count($_SESSION['cart']);

        foreach ($_SESSION['cart'] as $item => $quantity) {
            $request .= '&' . urlencode($item) . "&$quantity&" . count($_SESSION[$item]);
            foreach ($_SESSION[$item] as $selection)
                $request .= "&$selection";
        }
        $savecart_array = backend_query($request);
        $savecart_confirmation = $savecart_array[0];
        if($savecart_confirmation)
            report("Cart Saved", REPORT_INFO);
        else
            report("Problem saving cart.", REPORT_WARNING);
    }
    $total_price = 0;
    foreach ($_SESSION['cart'] as $item => $quantity) {
        if (empty($_SESSION['price'][$item])) {
            $request=BACKEND_CMD_GETPRICE . '&' . urlencode($item);
            foreach($_SESSION[$item] as $selection)
                $request .= "&$selection";
            $backend_price = backend_query($request);
            $_SESSION['price'][$item] = $backend_price[0];
        }
//        $total_price += $_SESSION['total_price'][$item] = $_SESSION['price'][$item] * $quantity;
        $_SESSION['total_price'][$item] = $_SESSION['price'][$item] * $quantity;
        $total_price += $_SESSION['total_price'][$item];
        $_SESSION['total_price'][$item] = number_format($_SESSION['total_price'][$item], 2);
    }

    $body->assign('total_price', number_format($total_price, 2));

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.cart.htm'));
    if(is_file(PADDING_DIR . 'cart'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'cart'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
