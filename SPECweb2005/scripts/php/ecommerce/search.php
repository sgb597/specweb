<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    $frame->assign('title', 'Search Results');

    // Check for required information
    checkGet('c', 'seg', 'kw');

    // Populate the body template
    $results = backend_query(BACKEND_CMD_SEARCH . "&$_GET[seg]&" . urlencode($_GET['kw']));
    foreach ($results as $key => $value) {
       $results[$key] = explode('&', $value);
    }

    $body->assign('items', $results);
    $body->assign('total_results', count($results));

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.search.htm'));
    if(is_file(PADDING_DIR . 'search'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'search'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
