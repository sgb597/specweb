<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    $frame->assign('title', 'Product Details');

    // Check for required information
    checkGet('s', 'c', 'i');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Populate the body template
    $details = backend_query(BACKEND_CMD_PRODUCT_DETAILS . "&$_GET[s]&" . urlencode($_GET['i']));
    $body->assign('details', $details);
    $body->assign('customer_types', $customer_types);

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.productdetail.htm'));
    if(is_file(PADDING_DIR . 'productdetail'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'productdetail'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
