<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('init_vars.php'); // server variables set by the prime client

    // Debugging and error handling
    define('REPORT_INFO',    0);
    define('REPORT_ERROR',   1);
    define('REPORT_WARNING', 2);

    // You don't need to modify the code from this point on
    define('BACKEND_CMD_SEARCH',          1);
    define('BACKEND_CMD_PRODUCT_LINE',    2);
    define('BACKEND_CMD_PRODUCT_MODELS',  3);
    define('BACKEND_CMD_PRODUCT_DETAILS', 4);
    define('BACKEND_CMD_CUSTOMIZE',       5);
    define('BACKEND_CMD_GETPRICE',        6);
    define('BACKEND_CMD_LOGIN',           7);
    define('BACKEND_CMD_REGISTER',        8);
    define('BACKEND_CMD_CARTSAVE',        9);
    define('BACKEND_CMD_SUBMIT_ORDER',   10);
    define('BACKEND_CMD_GET_REGIONS',    11);

    // Define customer types; short name (for form data) and long description (for displaying to user)
    $customer_types = array ('home' => 'Home use',
                             'smb'  => 'Small business',
                             'mlb'  => 'Medium/Large Business',
                             'govt' => 'Government/Healthcare'
                             );
    // Load in Smarty
    require_once(SMARTY_DIR.'Smarty.class.php');

    class SmartyECommerce extends Smarty {
        function SmartyECommerce(){
            $this->Smarty();
            $this->template_dir = SMARTY_ECOMMERCE_DIR . 'templates/';
            $this->compile_dir  = SMARTY_ECOMMERCE_DIR . 'templates_c/';
            $this->caching = false;
        }
    }

    // Initalize the frame template engine
    global $frame;
    global $report_log;
    $frame = new SmartyECommerce;
    $body  = new SmartyECommerce;

    // Initiate shared frame template variables
    $GLOBALS['variableDescription'] = array(
        's'   => "Customer Type",
        'c'   => "Region Code",
        'p'   => "Product Line",
        'i'   => "Product Item",
        'kw'  => "Key Word",
        'seg' => "Search Segment"
    );
    $frame->assign('variableDescription', $GLOBALS['variableDescription']);

  /* Function takes a BeSim request and parse the results.
     Return value: an array of the data (if no error),
                   or an empty array if an error occurred */
    function backend_query($request) {

    $besim_host = BESIM_HOST;
    $besim_port = BESIM_PORT;

    if (BESIM_COUNT > 1) // multiple BeSim case
    {
       global $besim_hosts, $besim_ports;
       $random_besim_num = rand(0, BESIM_COUNT - 1);
       $besim_host = $besim_hosts[$random_besim_num];
       $besim_port = $besim_ports[$random_besim_num];
    }

	if (strstr($request, '&') !== false)
	        list($cmd, $params) = explode('&', $request, 2);
	else {
		$cmd = $request;
		$params = '';
	}
        if ($cmd < 11)
            $request = "$cmd&" . ltrim($_GET['c'], 'ABCDEFGHIJKLMNOPQRSTUVWXYZ') . "&$params";
//        report('BeSim URI is ' . BESIM_URI."?2&$request", REPORT_INFO);

	if (!BESIM_PERSISTENT) {
           if (!$fp = fopen('http://'.$besim_host.':'.$besim_port.BESIM_URI."?2&$request", 'r')) {
              report('Could not open BeSim socket!', REPORT_ERROR);
              return array();
           }
           $response = '';
           while (!feof($fp)) {
              $response .= fgets($fp);
           }
           fclose($fp);
//           echo $response;
        } else { // BESIM_PERSISTENT == true
           if (!$fp = pfsockopen($besim_host, $besim_port))
           {
              report("Failed to open BeSim stream: $errstr ($errno)", REPORT_ERROR);
              return array();
           }
           fputs($fp, 'GET ' . BESIM_URI . "?2&$request HTTP/1.1\r\nHost: " . $besim_host . "\r\n\r\n");
           while ($line = fgets($fp)) { // read response line-by-line
              if (preg_match('/Content-Length: (.+)/i', $line, $matches)) {
	         $length = (int) trim($matches[1]); // length to fread
              } elseif ($line == "\r\n") { // end of HTTP headers
                 break;
              }
           }

           if (!isset($length) || $length == 0)
           {
              report("Could not read content length from BeSim!", REPORT_ERROR);
              return array();
           }

           $response = '';
           for ($bytes_read = 0; $bytes_read < $length; $bytes_read += strlen($response)) {
                $response .= fread($fp, $length);
           }
        } // end persistent

        if (preg_match('#<pre>\n(\d)\n(.*)\n</pre>#s', $response, $matches)) {
             $errno = $matches[1];
             if($errno != 0) {
                   report("BeSim returned with an error number: $errno", REPORT_ERROR);
                   return array();
             }
             $body = $matches[2];
             return explode("\n", $body);
        } else {
             report("Invalid response from BeSim: $response", REPORT_ERROR);
             return array();
        }
    }

    // Function checks to see if specified get values are set and errors if they are not
    function checkGet(){
        foreach(func_get_args() as $arg)
            if(!isset($_GET[$arg])) report("Required field {$arg}(<strong>" . $GLOBALS['variableDescription'][$arg] . "</strong>) not found.", REPORT_ERROR);
    }

    function report($what, $code){
        $codes = array('info', 'error', 'warning');

        $GLOBALS['report_log'][] = array(
            'type'    => $codes[$code],
            'message' => $what
        );

        if($code == REPORT_ERROR) renderPage();
    }

    // Function is called when script is finished populating the templates or encounters an error
    function renderPage() {
        $GLOBALS['frame']->assign('debug', $GLOBALS['report_log']);
        $html = $GLOBALS['frame']->fetch('tpl.frame.htm');
        if (SEND_CONTENT_LENGTH) header('Content-Length: ' . strlen($html));
        header('Cache-Control: no-cache');
        echo $html;
        exit;
    }

    function redirect($uri, $ssl = false) {
        header('Status: 307 Redirect');
        if (SEND_CONTENT_LENGTH) header('Content-Length: 0');
        if ($ssl && USE_SSL)
           header('Location: https://' . WEB_SERVER . ':' . SSL_PORT . dirname($_SERVER['PHP_SELF']). $uri);
        else
           header('Location: http://' . WEB_SERVER . ':' . WEB_PORT . dirname($_SERVER['PHP_SELF']). $uri);
        exit;
    }

function error_page($message)
{
    echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8858-1" />
	<link href="style.css" rel="stylesheet" type="text/css">
	<title>SPECweb2005: Ecommerce Error</title>
</head>
<body>
<!-- SPECweb2005 Error Page -->
<div class="error">
	<h1>Script Error</h1>
	<p>$message</p></div>
</body>
</html>
EOT;
    exit;
}

    session_start(); // Some servers do not automatically start the session
?>
