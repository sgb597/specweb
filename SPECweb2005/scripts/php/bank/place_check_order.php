<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  define("CHECK_PRICE", 1.0);

  function backend_place_check_order($userid, $account_no, $total_price){
    $request=BACKEND_CMD_PLACE_CHECK_ORDER.'&'.$userid.'&'.
             $account_no.'&'.strftime('%Y%m%d').'&'.$total_price;
    $r =backend_get_array($request);
    return $r;
  }

  check_login();
  $userid=$_SESSION['userid'];
  if(empty($_POST['number'])){
    show_msg("Empty number");
    exit();
  }else{
    $number=(int)$_POST['number'];
  }
  if(empty($_POST['check_design'])){
    show_msg("You did not select check design");
    exit();
  }else{
    $check_design=(int)$_POST['check_design'];
  }
  if(empty($_POST['account_no'])){
    show_msg("You did not specify account");
    exit();
  }else{
    $account_no=$_POST['account_no'];
  }
  if($number<=0){
    show_msg("Invalid number");
    exit();
  }
  if($check_design<=0 || $check_design>10){
    show_msg("Invalid design");
    exit();
  }
  $total_price=$number*CHECK_PRICE;
  list($status, $errno)=backend_place_check_order($userid, $account_no, $total_price);
  if($errno){
    show_msg("Error Code: $errno");
    exit();
  }
  $smarty=new SmartyBank;
  $smarty->assign('userid', $userid);
  $smarty->assign('msg', 'Check order placed successfully');
  $smarty->assign('conf', $status);
  $smarty->display('place_check_order.tpl');
?>
