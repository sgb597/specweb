<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  check_login();
  $userid=$_SESSION['userid'];
  $check_no=$_GET['check_no'];
  $side=$_GET['side'];
  if($side!='front' && $side!='back'){
    show_msg('Wrong side');
    exit();
  }
  if(empty($check_no)){
    show_msg('No empty check number allowed');
    exit();
  }
  list($check_image, $errno)=backend_query_check($userid, $check_no);
  if($errno==0){
    $front_image=$check_image[0][1];
    $back_image=$check_image[0][2];
    if($side=='front'){
      readfile($front_image);
    }else{
      readfile($back_image);
    }
  }else{
    show_msg("Error Code: $errno");
  }
?>

