<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Add Payee</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Add Payee</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
    <b>Please fill out ALL fields:</b>
    <form method="post" action="post_payee.php">
      <table cellpadding=3 border=1>
      <tr>
        <td><b>Nickname<br>
         (serve as id, no duplication):</b></td>
        <td><input type="text" name="payee_id"></td>
      <tr>
        <td><b>Name:</b></td>
        <td><input type="text" name="name"></td>
      <tr>
        <td><b>Street Address:</b></td>
        <td><textarea name="street" rows=3 cols=20></textarea></td>
      <tr>
        <td><b>City:</b></td>
        <td><input type="text" name="city"></td>
      <tr>
        <td><b>State:</b></td>
        <td><input type="text" name="state" size=2 maxlength=2></td>
      <tr>
        <td><b>Zip:</b></td>
        <td><input type="text" name="zip" size=5 maxlength=5></td>
      <tr>
        <td><b>Phone:</b></td>
        <td><input type="text" name="phone"></td>
      </table>
      <input type="submit" value="Submit">
    </form>
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"add_payee"}
</pre>
  </body>
</html>

