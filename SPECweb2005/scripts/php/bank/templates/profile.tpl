<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Profile</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <table summary="SPECweb2005_Profile" cellpadding=3>
    <tr>
      <th>Address</th>
      <td>{$profile.address|escape}</td>
    </tr>
    <tr>
      <th>City </th>
      <td>{$profile.city|escape}</td>
    </tr>
    <tr>
      <th>State </th>
      <td>{$profile.state|escape}</td>
    </tr>
    <tr>
      <th>Zip </th>
      <td>{$profile.zip|escape}</td>
    </tr>
    <tr>
      <th>Phone </th>
      <td>{$profile.phone|escape}</td>
    </tr>
    <tr>
      <th>Email </th>
      <td>{$profile.email|escape}</td>
    </tr>
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Profile</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
    <form method="post" action="change_profile.php">
      <table cellpadding=3>
        <tr>
          <td>Address</td>
          <td><textarea name="address" rows=3 cols=40></textarea></td>
	<tr>
	  <td>City </td>
	  <td><input type="text" name="city">
	</td>
	<tr>
	  <td>State </td>
	  <td><input type="text" name="state">
	</td>
	<tr>
	  <td>Zip </td>
	  <td><input type="text" name="zip">
	</td>
        <tr>
          <td>Phone </td>
          <td><input type="text" name="phone">
	</td>
        <tr>
          <td>Email </td>
          <td><input type="text" name="email">
	</td>
      </table>
      <input type="submit" value="Submit">
    </form>
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"profile"}
</pre>
  </body>
</html>
