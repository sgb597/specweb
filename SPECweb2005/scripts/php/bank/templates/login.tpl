<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Login</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
    <table summary="SPECweb2005_LoginMessage">
      <tr><th>Message</th></tr>
      <tr><td>{$msg}</td></tr>
    </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Login</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
    <form action="login.php" method="POST">
    <table border="0" align="center">
    <tr>
      <td align="right">User ID: <td><input type="text" name="userid">
    <tr>
      <td align="right">Password: <td><input type="password" name="password">
    <tr>
      <td align="center" colspan="2"><input type="submit" value="Login">
    </table>
    </form>
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
  </body>
</html>
