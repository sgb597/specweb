<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('init_vars.php'); // server variables set by the prime client

  define('BACKEND_CMD_LOGIN', 1);
  define('BACKEND_CMD_ACCT_BALANCE', 2);
  define('BACKEND_CMD_ACCT_SUMMARY', 3);
  define('BACKEND_CMD_CHECK_DETAIL',4);
  define('BACKEND_CMD_BILL_PAY', 5);
  define('BACKEND_CMD_POST_PAYEE', 6);
  define('BACKEND_CMD_QUICK_PAY', 7);
  define('BACKEND_CMD_REVIEW_BILL_PAY_STATUS', 8);
  define('BACKEND_CMD_PROFILE', 9);
  define('BACKEND_CMD_CHANGE_PROFILE', 10);
  define('BACKEND_CMD_PLACE_CHECK_ORDER', 11);
  define('BACKEND_CMD_POST_TRANSFER', 12);

  /* Define some internal error codes. BeSim errors will not
     be the same as these */
  define('ERROR_BESIM_OPEN', 1001);
  define('ERROR_BESIM_FORMAT', 1002);


  require_once(SMARTY_DIR.'Smarty.class.php');
  class SmartyBank extends Smarty{
    function SmartyBank(){
      $this->Smarty();
      $this->template_dir=SMARTY_BANK_DIR.'templates/';
      $this->compile_dir=SMARTY_BANK_DIR.'templates_c/';
      $this->config_dir=SMARTY_BANK_DIR.'configs/';
      $this->cache_dir=SMARTY_BANK_DIR.'cache/';
      $this->caching=false;
      $this->assign('PADDING_DIR', PADDING_DIR);
    }
  }

  /* Function takes a BeSim request and parse the results.
     Return value: an array of the data (if no error),
                   or an empty array if an error occurred */
    function backend_get_array($request) {

    $besim_host = BESIM_HOST;
    $besim_port = BESIM_PORT;

    if (BESIM_COUNT > 1) // multiple BeSim case
    {
       global $besim_hosts, $besim_ports;
       $random_besim_num = rand(0, BESIM_COUNT - 1);
       $besim_host = $besim_hosts[$random_besim_num];
       $besim_port = $besim_ports[$random_besim_num];
    }

	if (strstr($request, '&') !== false)
	        list($cmd, $params) = explode('&', $request, 2);
	else {
		$cmd = $request;
		$params = '';
	}

	if (!BESIM_PERSISTENT) {
           if (!$fp = fopen('http://'.$besim_host.':'.$besim_port.BESIM_URI."?1&$request", 'r')) {
              return array(array(), ERROR_BESIM_OPEN);
           }
           $response = '';
           while (!feof($fp)) {
              $response .= fgets($fp);
           }
           fclose($fp);
        } else { // BESIM_PERSISTENT == true
           if (!$fp = pfsockopen($besim_host, $besim_port))
           {
              return array(array(), ERROR_BESIM_OPEN);
           };
           fputs($fp, 'GET ' . BESIM_URI . "?1&$request HTTP/1.1\r\nHost: " . $besim_host . "\r\n\r\n");
           while ($line = fgets($fp)) { // read response line-by-line
              if (preg_match('/Content-Length: (.+)/i', $line, $matches)) {
	         $length = (int) trim($matches[1]); // length to fread
              } elseif ($line == "\r\n") { // end of HTTP headers
                 break;
              }
           }

           if (!isset($length) || $length == 0)
               return array(array(), ERROR_BESIM_OPEN);

           $response = '';
           for ($bytes_read = 0; $bytes_read < $length; $bytes_read += strlen($response)) {
                $response .= fread($fp, $length);
           }
        } // end persistent

        if (preg_match('#<pre>\n(\d)\n(.*)\n</pre>#s', $response, $matches)) {
             $errno = $matches[1];
             $body = explode("\n", $matches[2]);
             $r = array();
             foreach ($body as $line)
                  $r[] = explode('&', $line);
             return array($r, $errno);
        } else {
             return array(array(), ERROR_BESIM_FORMAT);
        }
    }

  function check_login(){
    if(empty($_SESSION['userid'])){
       $smarty=new SmartyBank;
       $smarty->assign('msg', 'Please login');
       $smarty->display('login.tpl');
       exit();
    }
  }

  function backend_query_check($userid, $check_no){
    $request=BACKEND_CMD_CHECK_DETAIL.'&'.$userid.'&'.$check_no;
    $a=backend_get_array($request);
    return $a;
  }

  function show_msg($msg){
    $smarty=new SmartyBank;
    $smarty->assign('msg', $msg);
    $smarty->display('message.tpl');
  }

  /* check if the date is in format yyyy-mm-dd
     and if the date is valid
     If it is valid, return a string yyyymmdd.
     otherwise, return FALSE */
  function date_to_int_string($date){
    if(ereg('^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$', $date, $array)){
      $year=$array[1];
      $month=$array[2];
      $day=$array[3];
      if(checkdate((int)$month, (int)$day, (int)$year)){
        return $year.$month.$day;
      }
    }
    return FALSE;
  }

  function backend_get_acct_balance($userid){
    $request=BACKEND_CMD_ACCT_BALANCE.'&'.$userid;
    list($a, $errno)=backend_get_array($request);
    return $a;
  }
 /* check if the date is in format yyyy-mm-dd
     and if the date is valid
     If it is valid, return an array(year, month, day)
     otherwise, return FALSE */
  function split_date($date){
    if(ereg('^([0-9]{4})-([0-9]{1,2})-([0-9]{1,2})$', $date, $array)){
      $year=(int)$array[1];
      $month=(int)$array[2];
      $day=(int)$array[3];
      if(checkdate($month, $day, $year)){
        return array($year, $month, $day);
      }
    }
    return FALSE;
  }

 function compare_date($year1, $month1, $day1, $year2, $month2, $day2){
//    print "$year1, $month1, $day1, $year2, $month2, $day2";
    if($year1<$year2){
      return -1;
    }elseif($year1>$year2){
      return 1;
    }
    if($month1<$month2){
      return -1;
    }elseif($month1>$month2){
      return 1;
    }
    if($day1<$day2){
      return -1;
    }elseif($day1>$day2){
      return 1;
    }else{
      return 0;
    }
  }







  /* converting the integer string returned by the
     backend into a date string.  I use iso-8601 standard
     YYYY-MM-DD. */
  function date_string($n){
    $r=substr($n, 0, 4).'-'.substr($n, 4, 2).'-'.substr($n, 6, 2);
    return $r;
  }

  function is_status_ok($s){
    return $s=='DONE Status=OK';
  }

  # Add appropriate response headers
  ob_start('send_response_headers');

  function send_response_headers($text){
    if (SEND_CONTENT_LENGTH) header('Content-Length: ' . strlen($text));
    header('Cache-Control: no-cache');
    return $text;
  }

  session_start();
?>
