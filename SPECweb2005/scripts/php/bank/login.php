<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  function backend_get_passwd($userid){
    $request=BACKEND_CMD_LOGIN.'&'.rawurlencode($userid);
    list($a, $errno)=backend_get_array($request);
    /* I ignored the error message.  Will the bank really disclose the
       error to the customer instead of just saying "wrong password or
       userid"? */
    if($errno==0){
      return $a[0][0];
    }else{
      return '';
    }
  }

  $smarty=new SmartyBank;
  $smarty->assign('msg','');
  if (!isset($_POST['userid']) && !isset($_POST['password'])){
    // new arrival
  }elseif (empty($_POST['userid']) || empty($_POST['password'])){
    $smarty->assign('msg', 'No empty user id or password allowed!');
  }else {
    $userid=$_POST['userid'];
    $passwd=md5($_POST['password']);
    $real_passwd=backend_get_passwd($userid);
    if($passwd==$real_passwd){
      $smarty->assign('userid', $userid);
      $acct_balance=backend_get_acct_balance($userid);
      $smarty->assign('acct_balance', $acct_balance);
      $smarty->display('welcome.tpl');
      $_SESSION['userid']=$userid;
      exit();
    }else{
      $smarty->assign('msg', 'Incorrect user id or password!');
//      $smarty->assign('msg', "Incorrect user id or password! $passwd != $real_passwd");
    }
  }
  $smarty->display('login.tpl');
?>
