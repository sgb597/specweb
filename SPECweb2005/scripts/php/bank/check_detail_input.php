<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  check_login();
  $smarty=new SmartyBank;
  $smarty->assign('userid', $_SESSION['userid']);
  $smarty->display('check_detail_input.tpl');
?>
