<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

	// Include required objects and functions
	require_once('inc.common.php');

	// Redirect to home page if required information is not present
    checkGET('q');

	// Initalize template engines
	$frameTemplate = new SmartySupport;
	$bodyTemplate  = new SmartySupport;

	// Connect to the datasource
    $search_results = backend_query(BACKEND_CMD_SEARCH .'&'. urlencode($_GET['q']));

	// Populate Templates
	$bodyTemplate->assign('productListing', $search_results);

	$frameTemplate->assign('title', "Search Results for $_GET[q]");
	$frameTemplate->assign('body', $bodyTemplate->fetch('page.search.htm'));

    if(is_file(PADDING_DIR . 'search'))
        $frameTemplate->assign('padding',  file_get_contents(PADDING_DIR . 'search'));
    else
        echo('Unable to locate padding file.');

	// Render the page to the browser
	$frameTemplate->display('tpl.main.htm');
?>
