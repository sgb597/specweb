Here are the basics for setting up SPECweb2005 Clients:

First, you should at minimum have version 1.4.1 of the java jdk and/or jre 
installed on the client machines running this code to take advantage of garbage 
collection optimizations in the more recent jvms. However, the latest JVMs 
(currently version 1.5.x) are strongly recommended for best performance.

Next, there are five different .config files used for SPECweb2005. There is a 
SPECweb_<workload>.config for each of the three workloads (Banking, Ecommerce, 
and Support) that contain both configurable and fixed parameters specific to 
the workload.  Additionally, Test.config contains parameters that are common 
to all workloads and includes configurable and fixed parameters. Lastly, 
Testbed.config contains the testbed hardware/software configuration properties.

Note that Test.config and the workload-specific configuration files mentioned 
above do not exist in a new installation. Instead, example workload-specific  
and Test configuration files are provided, and you will need to copy the sample
file most suited to your environment. Ex:

cp Test.Unix-PHP.config Test.config
cp SPECweb_Banking.Unix-PHP.config SPECweb_Banking.config
cp SPECweb_Ecommerce.Unix-PHP.config SPECweb_Ecommerce.config
cp SPECweb_Support.Unix-PHP.config SPECweb_Support.config

"specwebclient" is the SPECweb2005 client load generator. After editing the 
.config files to match your test environment, you need to start this process on 
all client machines.  From the directory where specwebclient.jar is installed, 
and at a command prompt type:
	
	 java -jar specwebclient.jar 

Starting "specwebclient" with tuned heap sizes and garbage collection 
parameters is also recommended. Specifically, for v1.4.1 of the jdk on a 
multi-processor client, using parallel and CMS garbage collection is strongly 
recommended. Example: 

	java -cp c:\sw2005 -Xms512m -Xmx512m -XX:+UseParNewGC \
		-XX:+UseConcMarkSweepGC -jar specwebclient.jar

The usage for specwebclient is:

java -jar specwebclient.jar  -h
Usage: java -jar specwebclient.jar [-v] [-p port] [-n hostname] 
	[-s servername] [-le errorlogfilename] [-lo stdoutlogfilename]

Command line flags:
   -v  Print version string and exit
   -p <port> Override default port number 
		(allows multiple instances on same machine)
   -n <hostname>  Override hostname (use Alias)
   -s <servername> Overrides WEB_SERVER
   -le <errorlogfilename>  redirects error messages
   -lo <stdoutlogfilename> redirects other logging messages


"specweb" is the SPECweb2005 test manager and is run on the Prime Client.  It
is started after all the "specwebclient" processes have be started.  Starting 
specweb should begin the test.  The "specweb" process exits at the end of the 
test, and the "specwebclient" process can be configured to terminate as well 
(see Test.config).  To start the "specweb" process, from the directory where 
specweb.jar is installed, at a command prompt type:

         java -jar specweb.jar

The usage for specweb is:

java -jar specweb.jar  -h
Usage: java -jar specweb.jar [-C] [-v] [-w workloadname] [-le errorlogfilename] 
       [-lo stdoutlogfilename]

Command line flags:
   -h  Print this message and exit
   -v  Print version string and exit
   -C  Overrides any non-compliant settings in the .config files with the
       default compliant values
   -w  Overides the TEST_TYPE in the config file (SPECweb_Banking, 
       SPECweb_Ecommerce, SPECweb_Support)
   -le <errorlogfilename>  redirects error messages
   -lo <stdoutlogfilename> redirects other logging messages


"reporter" is the SPECweb2005 report generator and is invoked by "specweb" at
the end of a test to produce .TXT and .HTML reports.  It can also be invoked to
create a merged .raw file from results from running the 3 workloads on a given
testbed for a submission to SPEC; as well as generating  combined .TXT and 
.HTML report files. Here is the usage for the reporter:

java -jar reporter.jar -h
SPECweb2005 Reporter 1.00
Copyright (c) 2005 Standard Performance Evaluation Corporation

Command line flags:
   -h  Print this message and exit
   -v  Print version string and exit
   -c  Combine 3 separate workload files into one submittable .raw file
   -R  Regenerate HTML and ASCII reports from combined rawfile

Example Usage:
--------------
Regenerate reports from a single raw file:
java -jar reporter.jar results\SPECweb_Support.20050323-102204.raw

Combine three compliant workload raw files into one submittable raw file:
java -jar reporter.jar -c bank.raw ecom.raw support.raw submit.raw

Regenerate reports from a previously combined raw file:
java -jar reporter.jar -R submit.raw



