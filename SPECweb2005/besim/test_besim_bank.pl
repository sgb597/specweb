#!/opt/perl/bin/perl
#
# Author: P.Smith       03/2004            
#			
# Invokation: test_besim_bank.pl <besim_script_request>
#
# Example: test_besim_bank.pl http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: test_besim_bank.pl http://bsm614:81/isapi-bin/besim_zisapi.api
#
#
#
# The LWP package is part of: libwww-perl-5.18
use LWP::Simple;
#

$besim_script_request = $ARGV[0];

@sample_queries = (
"?1&0&1097157010&1&2000&200&/www/bank/images&0",
"?1&1&88",
"?1&2&18",
"?1&3&157",
"?1&4&58&3",
"?1&5&38",
"?1&6&78&1&Insurance%20Inc.&8012%20Birch%20Ln&Middletown&NC&55566&123-555-1212",
"?1&7&149&1&2004-10-09&100.00",
"?1&8&38&2004057&20041007",
"?1&9&157",
"?1&10&81&2347%20Birch%20Ln&Littleton&PA&13919&customer770%40isp.net&phone%3D123-555-1212",
"?1&11&327&0866436362&20041007&600",
"?1&12&177&0807761921&50&0832942271&20041007"
);
#
#print @sample_queries . "\n";
print "\nTesting BESIM Requests for Banking Workload\n";


for ($k=0; $k < @sample_queries; $k++) {

	$request = $besim_script_request . $sample_queries[$k];
	print "\n\n\n" . $request  . "\n\n";
	getprint $request;
}
