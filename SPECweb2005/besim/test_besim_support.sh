#!/bin/bash
#
# Invocation: test_besim_support.sh <besim_script_request>
# Prerequisites: cURL (http://curl.haxx.se/) must be in the path.
#
# Example: test_besim_support.sh http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: test_besim_support.sh http://bsm614:81/isapi-bin/besim_zisapi.api
#

if [ ! -n "$1" ]
then
  echo "Usage:   `basename $0` http://<BeSim host>:<port>/<BeSim API>"
  echo "Example: `basename $0` http://bsm614:81/fcgi/besim_fcgi.fcgi"
  exit 1
fi

sample_queries=(
"?3&0&1079975569&500"
"?3&1"
"?3&2&5"
"?3&3&Pro+Home+PDA"
"?3&4&500"
"?3&5"
"?3&6&200"
"?3&7&2000&Desktops&Dutch&RNZX"
"?3&8&12345"
"?3&0&1079978064&1234"
"?3&8&1009"
)

echo -e "\nTesting BESIM Requests for Support Workload\n";

for i in "${sample_queries[@]}"
do
  echo -e "\n\n\n"
  echo -e "-----------------------------------------\n"
  echo -e "$i\n\n"
  curl -v "$1$i"
done
