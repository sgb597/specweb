/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */


/* Structure containing all the static variables (except for Inited and the
   few in DebugLog.  The use of this structure in this program is not 
   thread safe
   */
typedef struct _req_data_t {

  /* Filled in for every request */
  char *Buffer;			/* Buffer for all return data */
  int BufCurLen;	        /* Current length of characters in Buffer */
  char *Inbuf;			/* Buffer for taking POST input */
  int InbufLen;             	/* Length of Inbuf */
  char RemoteAddr[128];	/* address of client making request */
  char *QueryString;		/* Query -- excluding GET/POST/... part */
  char *QueryMethod;		/* see enumerated type ReqTypes */
  char CookieStr[64];	/* Cookie string -- NULL if none sent */
  char CadStr[32];	        /* CustomAd String */
  char FileName[PATH_MAX];	/* FileName for GET and POST to return */
  


} spec_data_t;

typedef struct _global_spec_data_t {
  char *VersionStr;		/* Version of this code */
  char ServerSoftware[128];	/* Name of server software */
  char ScriptName[128];		/* Name of script */
  char TopDir[PATH_MAX];	/* Top directory */
  int LenTopDir;		/* strlen of TopDir */
  int LenServerSoftware; 	/* strlen of ServerSoftware */
  int LenScriptName;		/* strlen of ScriptName */
  int Pid;			/* Process ID */
  int *TimePtr;			/* Zeus internal time */
  long *CurrentResetGeneration;
  long ResetGeneration;
  char PanicPage[256];  /* In case initialize fails */
} global_spec_data_t;  

typedef void *WebInputs_t;

