#!/opt/perl/bin/perl
#
# Author: P.Smith       03/2004            
#			
# Invokation: test_besim_support.pl <besim_script_request>
#
# Example: 
#	test_besim_support.pl http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: 
#	test_besim_support.pl http://bsm614:81/isapi-bin/besim_zisapi.api
#
#
#
# The LWP package is part of: libwww-perl-5.18
use LWP::Simple;
#

$besim_script_request = $ARGV[0];

@sample_queries = (
"?3&0&1079975569&500",
"?3&1",
"?3&2&5",
"?3&3&Pro+Home+PDA",
"?3&4&500",
"?3&5",
"?3&6&200",
"?3&7&2000&Desktops&Dutch&RNZX",
"?3&8&12345",
"?3&0&1079978064&1234",
"?3&8&1009"
);

#print @sample_queries . "\n";
print "\nTesting BESIM Requests for Ecommerce Workload\n";


for ($k=0; $k < @sample_queries; $k++) {

	$request = $besim_script_request . $sample_queries[$k];
	print "\n\n\n" . $request  . "\n\n";
	getprint $request;
}
