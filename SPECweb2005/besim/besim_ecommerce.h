/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Ecommerce Command Module 

    besim_ecommerce.h

*/

#if !defined(BESIM_ECOMMERCE_H_INCLUDED)

#define BESIM_ECOMMERCE_H_INCLUDED

#include "besim_common.h"
#include "md5.h"

int Ecommerce_Reset (char* Arg[], int ArgCnt, char* MsgTxt);	 
int Search (char* Arg[], int ArgCnt, char* MsgTxt);	
int ProductLines (char* Arg[], int ArgCnt, char* MsgTxt);	
int ProductModels (char* Arg[], int ArgCnt, char* MsgTxt);
int ProductDetails (char* Arg[], int ArgCnt, char* MsgTxt);	 
int CustomizationChoices (char* Arg[], int ArgCnt, char* MsgTxt);	
int GetPrice (char* Arg[], int ArgCnt, char* MsgTxt);	
int CheckoutLogin (char* Arg[], int ArgCnt, char* MsgTxt);
int CheckoutRegister (char* Arg[], int ArgCnt, char* MsgTxt); 
int SaveCart (char* Arg[], int ArgCnt, char* MsgTxt);	
int SubmitOrder (char* Arg[], int ArgCnt, char* MsgTxt);	
int GetRegions (char* Arg[], int ArgCnt, char* MsgTxt);
int Ecommerce_Undef (char* Arg[], int ArgCnt, char* MsgTxt);
int Ecommerce_Valid1stArg(int cmd, char * arg1, char* msg);

#define  ERROR_UNDEF_CMD "Undefined Command Type in Request\n"
#define  ERROR_BAD_ARG1 "Invalid Argument 1 (region) for Command Type\n"
#define  ERROR_BAD_ARGS "Invalid Arguments for Command Type\n"
#define  ERROR_BAD_ARGS2 "Invalid Arguments2 for Command Type\n"
#define  ERROR_CMD_FAILED "Command Failed\n"

#define DOECOMMERCE "DoEcommerceCommand() Called\n"

  int (*ecommerce_cmdFp[16])() = 
	{ Ecommerce_Reset,  Search,        ProductLines,  ProductModels,  
	  ProductDetails,   CustomizationChoices, GetPrice,     CheckoutLogin, 
	  CheckoutRegister, SaveCart,      SubmitOrder,   GetRegions,
	  Ecommerce_Undef,  Ecommerce_Undef, Ecommerce_Undef, Ecommerce_Undef };
			 

  int ecommerce_cmdArgCnt[16] = 
	{ 4,	 3,	2,	3,
	  3,	 4,	4,	2,
	  2, 	 5,	25,	0,
	  0,  	 0,	0,	0 };

#define VALID(v,n,x)  (((v>=n)&&(v<=x))?(1):(0))
			 

/*typedef struct _ecommerce_globals_t {
  int lastResetTime;
  int gflag;
  int Time;
  int Min_region;
  int Max_region;
  int Load;
  int Scaled_Load;
  int Num_Products;
  int Num_ProductLines;
  int Num_ModelLines;
  int Num_SearchResults;
  int region_range;
  char ResetDate[16];
} ecommerce_globals_t;*/

workload_globals_t Constants;
workload_globals_t *gEConstants = NULL;


    char *CreditCards[5] =
        {"GetAClue", "PassPort", "USswift", "ExpertVoucher", "PurchaseOrder"};
    char *ShipMethod[5] =
        {"Ground", "3-Day", "2nd-Day", "OverNite", "APO"};

#ifndef BESIM_ECOMMERCE_GLOBALS_PATH
#ifndef WIN32
#define BESIM_ECOMMERCE_GLOBALS_PATH "/tmp/besim_ecommerce.globals"
#else
#define BESIM_ECOMMERCE_GLOBALS_PATH "c:\\besim_ecommerce.globals"
#endif
#endif


#endif /*BESIM_ECOMMERCE_H_INCLUDED*/
