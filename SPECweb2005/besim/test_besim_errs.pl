#!/opt/perl/bin/perl
#
# Author: P.Smith       03/2004            
#			
# Invokation: test_besim_ecom.pl <besim_script_request>
#
# Example: test_besim_ecom.pl http://bsm614:81/fast-cgi/besim_fcgi.fcgi
# Example: test_besim_ecom.pl http://bsm614:81/isapi-bin/besim_zisapi.api
#
#
#
# The LWP package is part of: libwww-perl-5.18
use LWP::Simple;
#

$besim_script_request = $ARGV[0];

@sample_queries = (
"?2&0&1079975569&1&80&100",
"?abc",
"fred",
"",
"?1&dkfdsfk;jh"
);
###"?2&1&20&Seg1&Aone+2be+3c",
###"?2&2&20&Home",
###"?2&3&20&Home&PDA",
###"?2&4&20&Home&FastOne",
###"?2&5&20&Home&FastOne-23&2",
###"?2&5&01&smb&LCD-TVs01&1");
###
####"?2&6&20&FastOne-23&Option1=ABC-22&Option2=DEF-23",
####"?2&7&20&M.Y.Self@home.net",
####"?2&8&20&Mike&Self&&M.Y.Self@home.net&M.Y.Self@home.net",
####"?2&9&20&M.Y.Self@home.net&2&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11",
####"?2&9&20&M.Y.Self@home.net&4&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11&ThirdItem&9&0&FouthItem&4&4&Option1=123&Option2=asd&Option3=443539475&Option4=fred",
####"?2&10&20&M.Y.Self@home.net&Mike&Self&123%20Elm%20St&Townville&VA&12345&Mike&Self&123%20Elm%20St&Townville&VA&12345&123-345-4567&GoodCred&6011234900001234&07&2005&3&FastOne-23&1&2&Option1=ABC-22&Option2=DEF-23&OtherOne-11&5&1&Option1=QAS-11&FastTwo-23&1&2&Option1=ABC-22&Option2=DEF-23b"
####,"?2&0&1079975569&1&80&100",
####"?2&11"
#);
#
#print @sample_queries . "\n";
print "\nTesting BESIM Requests for Ecommerce Workload\n";


for ($k=0; $k < @sample_queries; $k++) {

	$request = $besim_script_request . $sample_queries[$k];
	print "\n\n\n" . $request  . "\n\n";
	getprint $request;
}
