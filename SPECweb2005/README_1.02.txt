SPECweb2005 Release 1.02 Description
----------------------------------

SPECweb2005 Release 1.02 is a minor release which fixes several issues found 
in the benchmark after the 1.01 release.  In incorporates all the changes
from 1.01 (see README_1.01.txt for details).

There are code changes to the client harness, JSP and PHP scripts, and minor
documentation updates.

Here is a detailed list of the code changes:

    o The client harness reporter (org/spec/specweb/reporter/) has been updated 
      to automatically verify that all quality criteria is met per section 2.2.1 
      of the Run and Reporting Rules.  Specifically, code has been added to check 
      that "The Weighted Percentage Difference (WPD) between the Expected Number 
      of Requests (ENR) and the actual number of requests (ANR) for any given 
      page is within +/- 1%.  The sum of the per page Weighted Percentage 
      Differences (SWPD) does not exceed  +/- 1.5%."  
      
    o The client harness has a fix for a possible NullPointerException if 
      clients are specified by IP address instead of hostname.

    o The ASCII and HTML client harness reporters had a hard-coded limit on the 
      number of client hosts; this has been removed.
      
    o The ecommerce JSP code (inc/_beginBoil.jsp) has been corrected to send a
      "</tr>" HTML tag.

    o The ecommerce PHP code (ecommerce/templates/tpl.frame.htm) attempted to
      reference several POST variable descriptions that were not defined.
      The references to {$variableDescription[$key]} for POST variables has
      been removed.

Here is a detailed list of the documentation changes:

    o The User's Guide has a new Appendix G which provides step-by-step
      instructions for installing and configuring BeSim on an Apache HTTP Server.
      There is also a new FAQ and other miscellaneous cleanups.
      
    o The Support workload design document has been updated to specify the
      byte rate quality of service criteria applied to the large download files.

    o The ecommerce pseudocode had several PHP/Smarty-specific references that
      have been replaced with more generic descriptions.  The unnecessary
      variableDescription lookups for POST variables has also been removed.
      
    o The run rules have had several minor corrections made:
       - The need in 2.2.1 to submit a spreadsheet along with the submission
         has been removed, as these checks are now done in the harness reporter.
       - Version strings updated to 1.02.
       - Clarified 3.2.7, bullet 4, to specify two additional *stable* releases
         must have been released.
      
Instructions for Installing the SPECweb2005 Release 1.02 Update Kit
-----------------------------------------------------------------

    o Rerun the installer (setup.jar or the Win32 EXE file) on all testbed
      components (prime client, clients, and SUT); BeSim is the exception, as
      it has not changed.  You can choose to install into a previous 1.00 or 1.01
      directory, although in this case you should back up your *.config files.

    o Be sure that no copies of the client are currently running.

    o Consider renaming your SPECweb2005 directories to SPECweb2005-1.02
      after the installation.
